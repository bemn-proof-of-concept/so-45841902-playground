import { Component } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  selectedFile: File = null;
  result = null;

  constructor(private http: HttpClient) { }

  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
  }

  processFile() {
    const fd = new FormData();
    fd.append('file', this.selectedFile, this.selectedFile.name);
    this.http.post('https://192.168.64.3:8001/api/ner', fd, {
      reportProgress: true,
      observe:'events'
    }).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        console.log("Progress: " + Math.round(event.loaded / event.total * 100))
      } else if (event.type === HttpEventType.Response) {
        console.log(event.body);
        this.result = JSON.stringify(event.body);
      }
      
    });
  }

}
